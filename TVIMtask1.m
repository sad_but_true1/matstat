%задаем исходные параметры задания №1
 
n=350; %объем выборки
sigma = sqrt(7.29); %среднеквадратическое отклонение
a = 2.6; %математическое ожидание
phi = 0.96; %доверительный интервал
 
%загружаем матрицу nx12 с выборкой равномерно распределенной случайной величины
load('datatask1.mat', 'A');
 
%формируем вектор-столбец nx1 нормально распределенной случайной величины с параметрами (a,sigma.^2)
temp = ones(n,1);
N = sigma*(sum(A, 2)-temp*6)+a*temp;
 
 %построение гистограммы
 %определение правой и левой границ построения
 left = min(N);
 right = max(N);
 %определим количество точек для разбиения с помощью правила Стургерса
 pointsCount = fix(1+3.32*log10(n))+1;
 %разобъем исходный промежуток на count точек
 intervalLength=(right-left)/(pointsCount);
 J = left:intervalLength:right;
 %запишем в вектор P частоты попадания выборочных значений в k-й интервал
 P = zeros(pointsCount,1);
 for i = 1:1:pointsCount
     P(i)=length(find((N>=J(i))&(N<J(i+1))));
 end
 %добавляем единицу, так как самая правая точка не вошла ни в один интервал по условию <
 P(pointsCount)=P(pointsCount)+1;

 %строим гистограмму
 
 hold on;
 bar(mean(J(1:2)):intervalLength:mean(J(pointsCount:pointsCount+1)), P/(n*intervalLength),1.0);
 x = -10: 0.1: 15;
 y = 1/(sigma * sqrt(2*pi)) * exp(- (x - a).^2/(2*(sigma).^2));
 plot (x,y);
 hold off;
 %hist(N, 10); %нужно корректно уменьшить высоту гистограммы

 %формируем данные таблицы, пока не хватает суммы частот
 dat=cell(pointsCount, 5);
 for i=1:1:pointsCount
     dat{i,1}=strcat('(',num2str(J(i)),'; ',num2str(J(i+1)),')'); %границы интервалов группировки
     dat{i,2}= P(i); %частоты
     dat{i,3}= P(i)/n; %относительные частоты
     dat{i,4}=P(i)/(n*intervalLength); %значения высот столбцов гистограммы
     dat{i,5}=(1/(sigma*sqrt(2*pi)))*exp(-(mean(J(i:i+1))-a).^2/(2*sigma.^2)); %теоретическая плотность вероятностей в серединах интервалов
 end
 sum(P) %сумма частот

 %выводим таблицу
 f=figure('Position', [0 0 800 400]);
 cNames = {'Границы интервалов', 'Частоты', 'Относительные частоты', 'Высоты столбцов гистограммы', 'Теоретическая плотность'};
 uitable('Parent', f, 'Data', dat, 'ColumnName', cNames, 'Position', [0 0 800 400]);

 %пункт 1.2 вычисление выборочного среднего и выборочной дисперсии
 XAV=mean(N); %выборочное среднее
 a; %математическое ожидание
 SAV=var(N,1); %выборочная дисперсия, аналог mean((N-mean(N)*ones(n,1)).^2)
 sigma^2; %дисперсия
 
%пункт 1.3

MX = sum(N)/n; %математическое ожидание по методу максимального правдоподобия
SIG2 = sum((N-a).^2)/n; %дисперсия по ммп
SIG2AV = sum((N-XAV).^2)/n; %смещённая оценка дисперсии
BSIG2 = n*SIG2AV/(n-1); %несмещенная оценка дисперсии
% SIG2 = (N(1) - a)^2;
% for i=2:1:n
%     SIG2=SIG2 + (N(i)-a)^2;
% end
% SIG2=SIG2/n;

%пункт 1.4
%доверительные интервалы для мат.ожидания

deltaphi1 = [XAV - norminv((1+phi)/2, 0, 1) * sigma/sqrt(n), XAV + norminv((1+phi)/2, 0, 1) * sigma/sqrt(n)];% неизвестно мат.ожидание и известна дисперсия
deltaphi2kvant = [XAV - norminv((1+phi)/2, 0, 1) * sqrt(SAV) / sqrt(n-1), XAV + norminv((1+phi)/2, 0, 1) * sqrt(SAV) / sqrt(n-1)];% неизвестны мат.ожидание и дисперсия
deltaphi2 = [XAV - icdf('t',(1+phi)/2, n-1) * sqrt(SAV) / sqrt(n-1), XAV + icdf('t',(1+phi)/2, n-1) * sqrt(SAV) / sqrt(n-1)];


%доверительные интервалы для дисперсии 
Cp1 = norminv((1+phi)/2, 0,1);%2.051;
Cp2 = -Cp1;%-2.051;
Hi2_1 = ((Cp1 + sqrt(2*n-1))^2)/2;
Hi2_2 = ((Cp2 + sqrt(2*n-1))^2)/2;
deltaphi3 = [sum((N-a).^2)/Hi2_1,sum((N-a).^2)/Hi2_2];% известно мат.ожидание

Hi2_3 = ((Cp1 + sqrt(2*(n-1) - 1))^2)/2;
Hi2_4 = ((Cp2 + sqrt(2*(n-1) - 1))^2)/2;

deltaphi4 = [n*SAV/Hi2_3,n*SAV/Hi2_4];% неизвестно мат.ожидание

%пункт 1.5
 %изначально используется разбиение из 1.1
 count=zeros (20, 1);
 ol=1;
     for i = 1:1:pointsCount
        if P(i) < 5
            count(ol)=count(ol)+1;
        end
     end
     pointsCount15 = pointsCount - count(ol);
     J15 = left:(right-left)/(pointsCount15):right;
     P15 = zeros(pointsCount15,1);
     for i = 1:1:pointsCount15
         P15(i)=length(find((N>=J15(i))&(N<J15(i+1))));
     end
     
 while count(ol) ~=0
     ol=ol+1;
     for i = 1:1:pointsCount15
        if P15(i) < 5
            count(ol)=count(ol)+1;
        end
     end
     pointsCount15 = pointsCount15 - count(ol);
     J15 = left:(right-left)/(pointsCount15):right;
     P15 = zeros(pointsCount15,1);
     for i = 1:1:pointsCount15
         P15(i)=length(find((N>=J15(i))&(N<J15(i+1))));
     end
 end
%переделать
 
 %вопрос какие дисперсию и мат. ожидание использовать
 p1 = zeros(pointsCount15, 1);
 p2 = zeros(pointsCount15, 1);
 pkr= zeros(pointsCount15, 1);
 
 for i = 2:1:pointsCount15+1
     p1(i-1) = normcdf(J15(i), XAV, sqrt(SAV)) - normcdf(J15(i-1), XAV, sqrt(SAV));
     p2(i-1) = normcdf(J15(i), a, sigma) - normcdf(J15(i-1), a, sigma);
     pkr(i-1) = P15(i-1)/n;
 end
 
 dat=cell(pointsCount15, 5);
 for i=1:1:pointsCount15
     dat{i,1}=strcat('(',num2str(J15(i)),'; ',num2str(J15(i+1)),')'); %границы интервалов группировки
     dat{i,2}= P15(i); %частоты
     dat{i,3}= pkr(i); %относительные частоты
     dat{i,4}= p1(i);
     dat{i,5}= p2(i);
 end
 f=figure('Position', [0 600 800 400]);
 cNames = {'Границы интервалов', 'Частоты', 'Относительные частоты', 'Теоретическая вероятность 1', 'Теоретическая вероятность 2'};
 uitable('Parent', f, 'Data', dat, 'ColumnName', cNames, 'Position', [0 0 800 400]);
%  p1m=1;
%  p2m=1;
%  for i= 1:1:pointsCount15+1
%     p1m=p1m*p1(i);
%     p2m=p2m*p2(i);
%  end
 hi2_15_1 = sum(((P15 - n*p1).^2)./(n*p1));
 hi2_15_2 = sum(((P15 - n*p2).^2)./(n*p2));
%  hi2_15_1 = 0;
%  hi2_15_2 = 0;
%  for i=1:1:pointsCount15+1
%      hi2_15_1=hi2_15_1 + (((P15(i) - ( n*p1(i) ) )^2)/n/p1(i));
%      hi2_15_2=hi2_15_2 + (((P15(i) - (n*p2(i)))^2)/(n*p2(i)));
%  end
 %???