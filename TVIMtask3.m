%исходные данные задания 3

n = 350;
MX = 2.6;
DX = 7.29;
MY = -5.6;
DY = 1.69;
rXY = -0.15;

load('datatask3X.mat', 'Xnd');
load('datatask3Y.mat', 'Ynd');

%пункт 1

XAV = sum(Xnd)/n;
YAV = sum(Ynd)/n;

SXAV = sum((Xnd - XAV).^2) / n;
SYAV = sum((Ynd - YAV).^2) / n;
rXYAV = (sum(Xnd .* Ynd) - n*XAV*YAV)/sqrt((sum((Xnd).^2)-n*XAV^2)*(sum((Ynd).^2)-n*YAV^2));

%пункт 2

 xleft = min(Xnd);
 xright = max(Xnd);
%определим количество точек для разбиения с помощью правила Стургерса
 xpointsCount = fix(1+3.32*log10(n))+1;
 %разобъем исходный промежуток на count точек
 xintervalLength=(xright-xleft)/(xpointsCount);
 xJ = xleft:xintervalLength:xright;
 
 yleft = min(Ynd);
 yright = max(Ynd);
%определим количество точек для разбиения с помощью правила Стургерса
 ypointsCount = fix(1+3.32*log10(n))+1;
 %разобъем исходный промежуток на count точек
 yintervalLength=(yright-yleft)/(ypointsCount);
 yJ = yleft:yintervalLength:yright;
 %запишем в вектор P частоты попадания выборочных значений в k-й интервал
 P = zeros(xpointsCount,ypointsCount);
 for i = 1:1:xpointsCount
     for j = 1:1:ypointsCount
         P(i,j) = length (find((Ynd>=yJ(j))&(Ynd<yJ(j+1))&(Xnd>=xJ(i))&(Xnd<xJ(i+1))));
     end;
 end
 P(xpointsCount,ypointsCount) = P(xpointsCount,ypointsCount) + 2;
 
 
 xP = zeros(xpointsCount,1);
 for i = 1:1:xpointsCount
     xP(i)=length(find((Xnd>=xJ(i))&(Xnd<xJ(i+1))));
 end
 %добавляем единицу, так как самая правая точка не вошла ни в один интервал по условию <
 xP(xpointsCount)=xP(xpointsCount)+1; 
 
 yP = zeros(ypointsCount,1);
 for i = 1:1:ypointsCount
     yP(i)=length(find((Ynd>=yJ(i))&(Ynd<yJ(i+1))));
 end
 %добавляем единицу, так как самая правая точка не вошла ни в один интервал по условию <
 yP(ypointsCount)=yP(ypointsCount)+1; 
 
 count = 0;
 for i = 1:1:xpointsCount
     for j = 1:1:ypointsCount
         if P(i,j) < 5
             count = count+1;
         end
     end
 end
 
 while count ~=0
     xpointsCount = xpointsCount - 1;
     xJ = xleft:(xright-xleft)/(xpointsCount):xright;
     P = zeros(xpointsCount,ypointsCount);
     for i = 1:1:xpointsCount
         for j = 1:1:ypointsCount
             P(i,j) = length (find((Ynd>=yJ(j))&(Ynd<yJ(j+1))&(Xnd>=xJ(i))&(Xnd<xJ(i+1))));
         end;
     end
     P(xpointsCount,ypointsCount) = P(xpointsCount,ypointsCount) + 2;
     count = 0;
     for i = 1:1:xpointsCount
         for j = 1:1:ypointsCount
             if P(i,j) < 5
                 count = count+1;
             end
         end
     end
     if count ~=0
         ypointsCount = ypointsCount -1;
         yJ = yleft:(yright-yleft)/(ypointsCount):yright;
         P = zeros(xpointsCount,ypointsCount);
         for i = 1:1:xpointsCount
             for j = 1:1:ypointsCount
                 P(i,j) = length (find((Ynd>=yJ(j))&(Ynd<yJ(j+1))&(Xnd>=xJ(i))&(Xnd<xJ(i+1))));
             end
         end
         P(xpointsCount,ypointsCount) = P(xpointsCount,ypointsCount) + 2;
         count = 0;
         for i = 1:1:xpointsCount
             for j = 1:1:ypointsCount
                 if P(i,j) < 5
                     count = count+1;
                 end
             end
         end
     end
 end
 
 xP = zeros(xpointsCount, 1);
 for i = 1:1:xpointsCount
     xP(i) = length(find((Xnd>=xJ(i))&(Xnd<xJ(i+1))));
 end
 
 yP = zeros(ypointsCount, 1);
 for i = 1:1:ypointsCount
     yP(i) = length(find((Ynd>=yJ(i))&(Ynd<yJ(i+1))));
 end
 xP(xpointsCount)=xP(xpointsCount)+1;
 yP(ypointsCount)=yP(ypointsCount)+1; 
 
 hi2 = -n;
 for i=1:1:xpointsCount
     for j=1:1:ypointsCount
         hi2 = hi2 + n * (P(i,j) ^ 2)/ (xP(i) * yP(j));
     end
 end
 
 
T = rXYAV * sqrt(n-1) /sqrt(1 - rXYAV ^ 2);
icdf ('t', 1 - 0.01/2, n-2) 
 
 %пункт 3
 y = rXYAV * (Xnd - XAV) * sqrt(SYAV/SXAV) + YAV;
 x = rXYAV * (Ynd - YAV) * sqrt(SXAV/SYAV) + XAV;
 
 hold on;
 plot (Xnd,y,'g');
 plot (Xnd, Ynd,'r+');
 hold off;
 figure;
 hold on;
 plot (x,Ynd,'b');
 plot (Xnd, Ynd,'r+');
 hold off;
 