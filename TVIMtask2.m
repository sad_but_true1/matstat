%параметры для второго задания
a_par = 2.2;
b_par = -1.5;
alpha = 0.025;
phi = 0.92;% дов. интервал
n = 250;% объем выборки

% аналитические значения
MX = 1/a_par + b_par;% мат ожидание
MX2=  b_par^2 + 2*b_par/a_par + 2 / (a_par^2);% мат ожидание квадрата
DX = MX2 - MX ^ 2;% дисперсия

load('datatask2.mat', 'X');

%пункт 2.1
%определение правой и левой границ построения
 left = min(X);
 right = max(X);
%определим количество точек для разбиения с помощью правила Стургерса
 pointsCount = fix(1+3.32*log10(n))+1;
 %разобъем исходный промежуток на count точек
 intervalLength=(right-left)/(pointsCount);
 J = left:intervalLength:right;
 %запишем в вектор P частоты попадания выборочных значений в k-й интервал
 P = zeros(pointsCount,1);
 for i = 1:1:pointsCount
     P(i)=length(find((X>=J(i))&(X<J(i+1))));
 end
 %добавляем единицу, так как самая правая точка не вошла ни в один интервал по условию <
 P(pointsCount)=P(pointsCount)+1;
 
 %строим гистограмму
 hold on;
 bar(mean(J(1:2)):intervalLength:mean(J(pointsCount:pointsCount+1)), P/(n*intervalLength),1.0);
 x = b_par: 0.1: 1;
 y = a_par * exp (-a_par*(x - b_par));
 plot (x,y);
 hold off;
 %hist(X,20);
 
 %формируем данные таблицы, пока не хватает суммы частот
 dat=cell(pointsCount, 5);
 for i=1:1:pointsCount
     dat{i,1}=strcat('(',num2str(J(i)),'; ',num2str(J(i+1)),')'); %границы интервалов группировки
     dat{i,2}= P(i); %частоты
     dat{i,3}= P(i)/n; %относительные частоты
     dat{i,4}=P(i)/(n*intervalLength); %значения высот столбцов гистограммы
     dat{i,5}=(1/(DX*sqrt(2*pi)))*exp(-(mean(J(i:i+1))-MX).^2/(2*DX.^2)); %теоретическая плотность вероятностей в серединах интервалов
 end
 sum(P) %сумма частот
 
 %выводим таблицу
 f=figure('Position', [0 0 800 400]);
 cNames = {'Границы интервалов', 'Частоты', 'Относительные частоты', 'Высоты столбцов гистограммы', 'Теоретическая плотность'};
 uitable('Parent', f, 'Data', dat, 'ColumnName', cNames, 'Position', [0 0 800 400]);
 
 %пункт 2.2
 XAV = 0;
 for i=2:1:pointsCount+1
     XAV = XAV + (J(i) + J(i-1))/2 * P(i-1);
 end
 XAV = XAV/n;% выборочное среднее
 XAV1 = sum (X)/n;
 
 SAV = 0;
 for i=2:1:pointsCount+1
     SAV = SAV + (((J(i) + J(i-1))/2 - XAV)^2) * P(i-1);
 end
 SAV = SAV/n;
 SAV1 = sum ((X - XAV).^2)./n;

%пункт 2.3
 amm=1/sqrt(SAV);
 bmm=XAV - 1/amm;
 
%пункт 2.4
 deltaphi1 = [XAV - norminv((1+phi)/2, 0, 1) * sqrt(SAV)/sqrt(n), XAV + norminv((1+phi)/2, 0, 1) * sqrt(SAV)/sqrt(n)];% неизвестно мат.ожидание и известна дисперсия
 
 M4 = sum ((X - XAV).^4)/n;
 deltaphi2 = [SAV - norminv((1+phi)/2, 0, 1) * sqrt(M4 - SAV ^2) / sqrt(n), SAV + norminv((1+phi)/2, 0, 1) * sqrt(M4 - SAV ^2) / sqrt(n)];% неизвестно мат.ожидание
 
%пункт 2.5
%изначально используется разбиение из 2.1
 count=zeros (20, 1);
 ol=1;
     for i = 1:1:pointsCount
        if P(i) < 5
            count(ol)=count(ol)+1;
        end
     end
     pointsCount25 = pointsCount - count(ol);
     J25 = left:(right-left)/(pointsCount25):right;
     P25 = zeros(pointsCount25,1);
     for i = 1:1:pointsCount25
         P25(i)=length(find((X>=J25(i))&(X<J25(i+1))));
     end
     
 while count(ol) ~=0
     ol=ol+1;
     for i = 1:1:pointsCount25
        if P25(i) < 5
            count(ol)=count(ol)+1;
        end
     end
     pointsCount25 = pointsCount25 - count(ol);
     J25 = left:(right-left)/(pointsCount25):right;
     P25 = zeros(pointsCount25,1);
     for i = 1:1:pointsCount25
         P25(i)=length(find((X>=J25(i))&(X<J25(i+1))));
     end
 end
%переделать
 
 p = zeros(pointsCount25, 1);
 pkr= zeros(pointsCount25, 1);
 
 for i = 2:1:pointsCount25+1
     p(i-1) = 1 - exp(a_par * (b_par - J25(i))) - (1 - exp(a_par * (b_par - J25(i-1))));
     pkr(i-1) = P25(i-1)/n;
 end
 
 dat=cell(pointsCount25, 4);
 for i=1:1:pointsCount25
     dat{i,1}=strcat('(',num2str(J25(i)),'; ',num2str(J25(i+1)),')'); %границы интервалов группировки
     dat{i,2}= P25(i); %частоты
     dat{i,3}= pkr(i); %относительные частоты
     dat{i,4}= p(i);
 end
 f=figure('Position', [0 600 800 400]);
 cNames = {'Границы интервалов', 'Частоты', 'Относительные частоты', 'Теоретическая вероятность'};
 uitable('Parent', f, 'Data', dat, 'ColumnName', cNames, 'Position', [0 0 800 400]);
 
 hi2_25 = sum(((P25 - n*p).^2)./(n*p));